package org.zerock.domain;

import lombok.Data;
import lombok.ToString;

//@Getter
//@Setter
//@ToString
@Data // getter/setter, toString() 작성 , ORM : 객체와 객체가 관계를 가지는 조합 조심
@ToString(exclude= {"val3"})
public class SampleVO {
	
	private String val1;
	private String val2;
	private String val3;
}
