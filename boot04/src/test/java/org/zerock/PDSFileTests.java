package org.zerock;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.zerock.persistence.PDSFileRepository;

import lombok.extern.java.Log;

@SpringBootTest
@Log    // lombok 의 로그를 사용
@Commit // test 결과를 데이터베이스에 commit
public class PDSFileTests {
	@Autowired
	PDSFileRepository repo;
	
	@Transactional
	@Test
	public void testUpdateFileName1() {
		Long fno = 1L;
		String newName = "updatedFile1.doc";
		
		int count = repo.updatePDSFile(fno, newName);
		log.info("update count: " + count);
	}
}
