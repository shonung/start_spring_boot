package org.zerock;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.zerock.domain.FreeBoard;
import org.zerock.persistence.FreeBoardReplyRepository;
import org.zerock.persistence.FreeBoardReposiroty;

import lombok.extern.java.Log;

@SpringBootTest
@Log    // lombok 의 로그를 사용
@Commit
public class FreeBoardTests {
	@Autowired
	FreeBoardReposiroty boardRepo;
	
	@Autowired
	FreeBoardReplyRepository replyRepo;
	
	@Test
	public void insertDummy() {
		
		IntStream.range(1,200).forEach(i -> {
			FreeBoard board = new FreeBoard();
			board.setTitle("Free Board..." + i);
			board.setContent("Free Content... " + i);
			board.setWriter("user" + i%10);
			
			boardRepo.save(board);
		});
	}
}
